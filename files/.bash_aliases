#Git
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '

#Applications 
alias subl='~/.local/share/umake/ide/sublime-text/sublime_text'
alias idea='~/.local/share/umake/ide/idea/bin/idea.sh'